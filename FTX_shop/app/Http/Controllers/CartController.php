<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Product;
use DB;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function view(){
        $listcat = DB::table('categories')
            ->where('parent_id', 0)
            ->get();

        $datau = [];
        if (Session::has('LoginId')) {
            $datau = DB::table('users')
                ->where('id', Session::get('LoginId'))
                ->first();
        }
        return view('user.cart',['listcat'=>$listcat,'datau'=>$datau]);
    }

    public function add(Cart $cart,$id){
        $product= DB::table('products')->find($id);
        $cart->add($product);
        return redirect()->back();

    }
    public function remove(Cart $cart,$id){
        $cart->remove($id);
        return redirect()->back();
    }
    public function update(Cart $cart,$id){
        $quantity=request()->quantity ?request()->quantity : 1;
        $cart->updates($id,$quantity);
        return redirect()->back();
    }
    public function clear(Cart $cart){
        $cart->clear();
        return redirect()->back();
    }
}
