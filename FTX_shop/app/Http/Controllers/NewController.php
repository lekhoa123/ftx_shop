<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddNewsRequest;
use App\Http\Requests\EditNewsRequest;
use App\Models\Category;
use App\Models\News;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Session;
use DateTime;

class NewController extends Controller
{
    public function list()
    {
        $data = News::paginate(5);
        return view('admin.new.list',['data'=>$data]);
    }

    public function add()
    {
        return view('admin.new.add');
    }
    public function postAdd(Request $request)
    {
        $n = new News();
        $n->title = $request->title;
        $n->slug = Str::slug($request->title,'-');
        $n->status = $request->status;
        $n->content = $request->content;

        if(Session::has('LoginId'))
        {
            $data =User::where('id','=',Session::get('LoginId'))->first();
        }
        $n->user_id=$data->id;
        $n->created_at = new datetime;

        $f = $request->file('img')->getClientOriginalName();
        $filename = time().'_'.$f;
        $n->image = $filename;
        $request->file('img')->move('uploads/news/',$filename);

        $n->save();
        return redirect()->route('new.list')
            ->with(['flash_level'=>'result_msg','flash_massage'=>' Đã thêm thành công !']);
    }
    public function edit($id)
    {
        $n = News::where('id',$id)->first();
        return view('admin.new.edit',['data'=>$n]);
    }
    public function postedit(Request $request,$id)
    {
        $n = News::find($id);
        $n->title = $request->title;
        $n->slug = Str::slug($request->txtTitle,'-');
        $n->status = $request->status;
        $n->content = $request->content;
        $n->updated_at = new datetime;


        $file_path = public_path('uploads/news/').$n->image;
        if ($request->hasFile('img')) {
            if (file_exists($file_path))
            {
                unlink($file_path);
            }
            $f = $request->file('img')->getClientOriginalName();
            $filename = time().'_'.$f;
            $n->image = $filename;
            $request->file('img')->move('uploads/news/',$filename);
        }

        $n->save();
        return redirect()->route('new.list')
            ->with(['flash_level'=>'result_msg','flash_massage'=>' Đã sửa thành công !']);
    }
    public function del($id)
    {
        $new = News::find($id);
        $new->delete();
        return redirect()->route('new.list')
            ->with(['flash_level'=>'result_msg','flash_massage'=>'Đã xóa !']);
    }

}
