<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\User;
use Illuminate\Http\Request;
use mysql_xdevapi\Table;
use Session;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $listcat = DB::table('categories')
            ->where('parent_id', 0)
            ->get();

        $pro = DB::table('products')
            ->select('products.*', 'pro_details.ram', 'pro_details.screen', 'pro_details.cpu', 'pro_details.storage', 'pro_details.note',)
            ->join('pro_details', 'products.id', '=', 'pro_details.pro_id')
            ->paginate(6);
        if( $key5=request()->key5){
            {
            $pro = DB::table('products')
                ->where('products.name','like','%'.$key5.'%')
                ->select('products.*', 'pro_details.ram', 'pro_details.screen', 'pro_details.cpu', 'pro_details.storage', 'pro_details.note',)
                ->join('pro_details', 'products.id', '=', 'pro_details.pro_id')
                ->paginate(6);
            }
        }

        $datau = [];
        if (Session::has('LoginId')) {
            $datau = DB::table('users')
                ->where('id', Session::get('LoginId'))
                ->first();
        }

        return view('user.home', ['listcat' => $listcat, 'pro' => $pro, 'datau' => $datau]);
    }

    public function pagecat($id)
    {
        $listcat = DB::table('categories')
            ->where('parent_id', 0)
            ->get();
        $datau = [];
        if (Session::has('LoginId')) {
            $datau = DB::table('users')
                ->where('id', Session::get('LoginId'))
                ->first();
        }
        $pro = DB::table('products')
            ->select('products.*', 'pro_details.ram', 'pro_details.screen', 'pro_details.cpu', 'pro_details.storage', 'pro_details.note', 'categories.id as ids')
            ->join('categories', 'products.cat_id', '=', 'categories.id')
            ->join('pro_details', 'products.id', '=', 'pro_details.pro_id')
            ->where('categories.parent_id', $id)
            ->paginate(3);
        if( $key5=request()->key5){
            {
                $pro = DB::table('products')
                    ->select('products.*', 'pro_details.ram', 'pro_details.screen', 'pro_details.cpu', 'pro_details.storage', 'pro_details.note', 'categories.id as ids')
                    ->join('categories', 'products.cat_id', '=', 'categories.id')
                    ->join('pro_details', 'products.id', '=', 'pro_details.pro_id')
                    ->where('categories.parent_id', $id)
                    ->where('products.name','like','%'.$key5.'%')
                    ->paginate(3);
            }
        }

        return view('user.catpage', ['listcat' => $listcat, 'pro' => $pro, 'datau' => $datau]);
    }
    public function pagenew(){
        $listcat = DB::table('categories')
            ->where('parent_id', 0)
            ->get();

        $datau = [];
        if (Session::has('LoginId')) {
            $datau = DB::table('users')
                ->where('id', Session::get('LoginId'))
                ->first();
        }

        $new=DB::table('news')
            ->orderBy('created_at', 'desc')
            ->paginate(1);
        return view('user.page.new',['listcat' => $listcat,'new'=>$new, 'datau' => $datau]);
    }


}

