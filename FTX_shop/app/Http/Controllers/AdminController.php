<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;
class AdminController extends Controller
{
    public function list()
    {
        $data = User::where('level','1')->paginate(5);
        return view('admin.admin.list',['data'=>$data]);
    }
    public function home()
    {

        return view('admin.home');
    }
    public function showFormLogin()
    {
        return view('admin.login');
    }

    public function postLogin(Request $request)
    {
        $request->validate([
            'email'=>'required|email',
            'password'=>'required|min:5|max:12'
        ],[
            'email.required'=>'Email bắt buộc phải nhập',
            'email.email'=>'Email phải đúng định dạng',
            'password.required'=>'Mật khẩu bắt buộc phải nhập',
            'password.min'=>'Mật khẩu ít nhất 5 kí tự',
            'password.max'=>'Mật khẩu nhiều nhất 12 kí tự',

        ]);
        $user = User::where('email', '=', $request->email)->first();
        if ($user) {
            if (hash::check($request->password, $user->password) && $user->level == 1) {
                $request->session()->put('LoginId', $user->id);
                return redirect()->route('admin.home');
            }

            return redirect()->route('admin.login')->with('fail','Thông tin đăng nhâp không đúng');
        }
    }
    public function logout(){
        if(Session::has('LoginId'))
        {
            Session::pull('LoginId');
            return view('admin.login');
        }
    }
}
