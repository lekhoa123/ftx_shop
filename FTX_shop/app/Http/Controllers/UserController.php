<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\hash;
use Session;
use DB;

class UserController extends Controller
{

    public function register()
    {
        return view('user.register');
    }
    public function postRegister(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'pass'=>'required|min:5|max:12'
        ],[
            'name.required'=>'Tên bắt buộc phải nhập',
            'email.required'=>'Email bắt buộc phải nhập',
            'email.email'=>'Email phải đúng định dạng',
            'email.unique'=>'Email đã tồn tại',
            'pass.required'=>'Mật khẩu bắt buộc phải nhập',
            'pass.min'=>'Mật khẩu ít nhất 5 kí tự',
            'pass.max'=>'Mật khẩu nhiều nhất 12 kí tự',

        ]);
        $user = new User();
        $user->name =$request->name;
        $user->email = $request->email;
        $user->password = hash::make($request->pass);
        $user->level=0;
        $user->status=1;

        $res=$user->save();
        if ($res){
            return back()->with('success','Bạn đã đăng kí thành công');
        }else{
            return back()->with('fail','Co mot so loi');
        }
    }

    public function login()
    {

        return view('user.login');
    }

    public function postLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:5|max:12'
        ], [
            'email.required' => 'Email bắt buộc phải nhập',
            'email.email' => 'Email phải đúng định dạng',
            'password.required' => 'Mật khẩu bắt buộc phải nhập',
            'password.min' => 'Mật khẩu ít nhất 5 kí tự',
            'password.max' => 'Mật khẩu nhiều nhất 12 kí tự',

        ]);

        $user =User::where('email','=',$request->email)->first();
        if ($user){
            if(hash::check($request->password,$user->password)){
                $request->session()->put('LoginId',$user->id);
                return redirect()->route('home');
            }
            else{
                return back()->with('fail','Mật khẩu chưa được đăng kí');
            }
        }
        else{
            return back()->with('fail','Email chưa được đăng kí');
        }
    }
    public function logout(){
        if(Session::has('LoginId'))
        {
            Session::pull('LoginId');
            return  redirect()->route('home');
        }
    }
    public function list()
    {
        $data = User::where('level','0')->paginate(5);
        return view('admin.user.list',['data'=>$data]);
    }
    public function info(){
//        $dataI=DB::table('users')
//            ->join('orders','users.id','=','orders.user')
//            ->where('users.id',Session::get('LoginId'));
        $dataU=DB::table('users')
            ->where('users.id',Session::get('LoginId'))
             ->first();
        $dataOr =[];
//        $dataOr=DB::table('users')
//            ->join('orders','users.id','=','orders.user_id')
//            ->where('users.id',Session::get('LoginId'))
//            ->first();
        $dataOr=DB::table('orders')
            ->select(DB::raw('SUM(total_money) as tongtien'),DB::raw('SUM(qty) as tongsl'))
            ->groupBy('user_id')
            ->having('user_id',Session::get('LoginId'))
            ->first();
//dd($dataOr);



        $listcat = DB::table('categories')
            ->where('parent_id', 0)
            ->get();

        $datau = [];
        if (Session::has('LoginId')) {
            $datau = DB::table('users')
                ->where('id', Session::get('LoginId'))
                ->first();
        }
        return view('user.info',['dataU'=>$dataU,'dataOr'=>$dataOr,'listcat'=>$listcat,'datau'=>$datau]);
    }
}
