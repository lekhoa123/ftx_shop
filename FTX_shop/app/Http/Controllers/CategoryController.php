<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DateTime;

class CategoryController extends Controller
{
    public function list(){
        $data = Category::all();
        return View ('admin.category.list',['data'=>$data]);
    }
    public function add(){
        $data = Category::all();
        return View ('admin.category.add',['data'=>$data]);
    }
    public function postAdd(Request $request){
        $cat = new Category();
        $cat->parent_id= $request->sltCate;
        $cat->name= $request->name;
        $cat->slug = Str::slug($request->name, '-');
        $cat->created_at = new DateTime;
        $cat->save();
        return redirect()->route('category.list')
            ->with(['flash_level'=>'result_msg','flash_massage'=>' Đã thêm thành công !']);
    }
    public function edit($id)   {
        $cat = Category::all();
        $data = Category::findOrFail($id)->toArray();
        return view('admin.category.edit',['cat'=>$cat,'data'=>$data]);
    }
    public function postEdit($id,Request $request)
    {
        $cat = Category::find($id);
        $cat->name = $request->name;
        $cat->slug = Str::slug($request->name,'-');
        $cat->parent_id = $request->sltCate;
        $cat->updated_at = new DateTime;
        $cat->save();
        return redirect()->route('category.list')
            ->with(['flash_level'=>'result_msg','flash_massage'=>' Đã sửa']);

    }
    public function del($id)
    {
        $parent_id = Category::where('parent_id', $id)->count();
        if ($parent_id == 0) {
            $category = category::find($id);
            $category->delete();
            return redirect()->route('category.list')
                ->with(['flash_level' => 'result_msg', 'flash_massage' => 'Đã xóa !']);
        } else {
            echo '<script type="text/javascript">
                  alert("Không thể xóa danh mục này !");
                window.location = "';
            echo route('category.list');
            echo '";
         </script>';
        }
    }
}
