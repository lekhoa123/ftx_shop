<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OtherController extends Controller
{
    public function pro_selling(Request $request)
    {
        $data = DB::table('products')->limit(5)
            ->join('order_details', 'products.id', '=', 'order_details.pro_id')
            ->select('products.id', 'products.name', 'products.images', DB::raw('SUM(qty) as tongsl'))
            ->groupBy('products.id', 'products.name'
                , 'products.images')
            ->orderBy('tongsl', 'desc')
            ->get();

        if ($key2 = request()->key2) {
            $data = DB::table('products')->limit(5)
                ->whereYear('order_details.created_at', 'like', $key2)
                ->join('order_details', 'products.id', '=', 'order_details.pro_id')
                ->select('products.id', 'products.name', 'products.images', DB::raw('SUM(qty) as tongsl'))
                ->groupBy('products.id', 'products.name'
                    , 'products.images')
                ->orderBy('tongsl', 'desc')
                ->get();

        }
        return view('admin.other.pro_selling', ['data' => $data]);
    }
    public function qty_buy(Request $request)
    {
        $data = DB::table('users')->limit(5)
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->select('users.id', 'users.name', 'users.email', DB::raw('SUM(qty) as tongsl'))
            ->groupBy('users.id','users.name','users.email')
            ->orderBy('tongsl', 'desc')
            ->get();

        if ($key2 = request()->key2) {
            $data = DB::table('users')->limit(5)
                ->join('orders', 'users.id', '=', 'orders.user_id')
                ->whereYear('orders.created_at', 'like', $key2)
                ->select('users.id', 'users.name', 'users.email', DB::raw('SUM(qty) as tongsl'))
                ->groupBy('users.id','users.name','users.email')
                ->orderBy('tongsl', 'desc')
                ->get();

        }
        return view('admin.other.user_total_qty_buy', ['data' => $data]);
    }
    public function money_buy(Request $request)
    {
        $data = DB::table('users')->limit(5)
            ->join('orders', 'users.id', '=', 'orders.user_id')
            ->select('users.id', 'users.name', 'users.email', DB::raw('SUM(total_money) as tongtien'))
            ->groupBy('users.id','users.name','users.email')
            ->orderBy('tongtien', 'desc')
            ->get();

        if ($key2 = request()->key2) {
            $data = DB::table('users')->limit(5)
                ->join('orders', 'users.id', '=', 'orders.user_id')
                ->whereYear('orders.created_at', 'like', $key2)
                ->select('users.id', 'users.name', 'users.email', DB::raw('SUM(total_money) as tongtien'))
                ->groupBy('users.id','users.name','users.email')
                ->orderBy('tongtien', 'desc')
                ->get();

        }
        return view('admin.other.user_total_money_buy', ['data' => $data]);
    }
}
