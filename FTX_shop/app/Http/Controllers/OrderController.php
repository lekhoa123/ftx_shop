<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DateTime;

class OrderController extends Controller
{
    public function list()
    {
        $data = Order::paginate(5);
        return view('admin.order.list',['data'=>$data]);
    }
    public function edit($id)
    {
        $n = Order::where('id',$id)->first();
        return view('admin.order.edit',['data'=>$n]);

//        $data = Order::where('id',$id);
//        return view('admin.order.edit',['data'=>$data]);
    }
    public function postEdit(Request $request, $id)
    {
        $o = Order::find($id);
        $o->name = $request->name;
        $o->email = $request->email;
        $o->address = $request->address;
        $o->status = $request->status;

        $o->updated_at = new datetime;

        $o->save();
        return redirect()->route('admin.order.list')
            ->with(['flash_level'=>'result_msg','flash_massage'=>' Đã sửa thành công !']);
    }
}
