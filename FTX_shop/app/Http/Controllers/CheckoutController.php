<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Order_detail;
use App\Models\User;
use Illuminate\Http\Request;
use Session;
use DB;
use DateTime;
class CheckoutController extends Controller
{
    public function form(){
        $data=array();
        if(Session::has('LoginId'))
        {
            $data =User::where('id','=',Session::get('LoginId'))->first();
        }
        else{
            return view('user.login');
        }

        $listcat = DB::table('categories')
            ->where('parent_id', 0)
            ->get();
        $datau = [];
        if (Session::has('LoginId')) {
            $datau = DB::table('users')
                ->where('id', Session::get('LoginId'))
                ->first();
        }

        return view('user.checkBuy.formCheck',['data'=>$data,'datau'=>$datau,'listcat'=>$listcat]);
    }

    public function submit_form(Request $req, Cart $cart)
    {
        $data = array();
        if (Session::has('LoginId')) {
            $data = User::where('id', '=', Session::get('LoginId'))->first();
        }
        $c_id = $data->id;
        $created_or=new datetime();
        if ($order = Order::create([
            'user_id' => Session::get('LoginId'),
            'status' => 0,
            'name'=>$req->name,
            'email'=>$req->email,
            'address'=>$req->address,
            'note' => $req->note,
            'qty'=> $cart->total_quanty,
            'total_money'=>$cart->total_price,
            'created_at'=>$created_or,

        ])) {
            $order_id = $order->id;

            foreach ($cart->items as $product_id => $item) {
                $quantity = $item['quantity'];
                $price = $item['price'];
                Order_detail::create([
                    'order_id' => $order_id,
                    'pro_id' => $product_id,
                    'qty' => $quantity,
                    'created_at'=>$created_or,
                ]);

            }
            session(['cart' => '']);
            return redirect()->route('checkout.success')->with('success', 'Đặt hàng thành công');
        } else {

            return redirect()->back()->with('error', 'Đặt hàng thất bại');
        }

    }

    public function success()
    {
        $listcat = DB::table('categories')
            ->where('parent_id', 0)
            ->get();
        $datau = [];
        if (Session::has('LoginId')) {
            $datau = DB::table('users')
                ->where('id', Session::get('LoginId'))
                ->first();
        }
        return view('user.checkBuy.checkout-success',['datau'=>$datau,'listcat'=>$listcat]);
    }

}
