<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddProductsRequest;
use App\Models\Category;
use App\Models\ProDetail;
use App\Models\Detail_img;
use App\Models\Pro_Detail;
use App\Models\Product;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use DateTime;

class ProductController extends Controller
{
    public function list($id)
    {
        if ($id=='all') {
//            $pro = Product::paginate(5);
            $pro = DB::table('products')
                ->select('products.*','categories.name as tendm')
                ->join('categories','products.cat_id','=','categories.id')->paginate(5);
            $cat= Category::all();
            return view('admin.product.list',['data'=>$pro,'cat'=>$cat,'loai'=>0]);
        }
        else {
            $cats= Category::all();
            $cat=Category::where('id',$id)->first();
            if ($cat->parent_id > 0) {
                $pro = DB::table('products')
                    ->select('products.*','categories.name as tendm')
                    ->join('categories','products.cat_id','=','categories.id')
                    ->where('cat_id', $id)
                    ->paginate(5);
                return view('admin.product.list', ['data' => $pro, 'loai' => $id, 'cat' => $cats]);
            } else {
                $cat = Category::where('parent_id', $cat->id)->get();
                $ids = $cat->pluck('id')->toArray();
                $pro = DB::table('products')
                    ->select('products.*','categories.name as tendm')
                    ->join('categories','products.cat_id','=','categories.id')
                    ->whereIn('cat_id', $ids)
                    ->paginate(5);
                return view('admin.product.list', ['data' => $pro, 'cat' => $cats, 'loai' => $id]);
            }
        }
    }
    public function add($id)
    {
        $loai = Category::where('id',$id)->first();
        $p_id = $loai->parent_id;
        $p_name = Category::where('id',$p_id)->first();
        $cat= Category::where('parent_id',$p_id)->get();
        $pro = Product::all();
        return view('admin.product.add',['data'=>$pro,'cat'=>$cat,'loai'=>$p_name->name]);

    }
    public function postAdd(Request $request)
    {
        $pro = new Product();

        $pro->name = $request->name;
        $pro->slug = Str::slug($request->name,'-');
        $pro->intro = $request->intro;
        $pro->price = $request->price;
        $pro->cat_id = $request->sltCate;
        $pro->created_at = new datetime;
        $pro->status = '1';



        $f = $request->file('img')->getClientOriginalName();
        $filename = time().'_'.$f;
        $pro->images = $filename;
        $request->file('img')->move('uploads/products/',$filename);
        $pro->save();

        $pro_id =$pro->id;
        $detail = new Pro_detail();
        $detail->cpu = $request->cpu;
        $detail->ram = $request->ram;
        $detail->screen = $request->screen;
        $detail->storage = $request->storage;
        $detail->note = $request->note;
        $detail->pro_id = $pro_id;
        $detail->created_at = new datetime;
        $detail->save();

        return redirect('admin/product/list/all')
            ->with(['flash_level'=>'result_msg','flash_massage'=>' Đã thêm thành công !']);

    }
    public function edit($id)
    {
        $dt = Product::where('id',$id)->first();
        $c_id= $dt->cat_id;
        $loai= Category::where('id',$c_id)->first();
        $p_id = $loai->parent_id;
        $cat= Category::where('parent_id', $p_id)->get();

        $pro = DB::table('products')
            ->select('products.*','pro_details.ram as ram','pro_details.cpu as cpu','pro_details.screen as screen','pro_details.storage as storage','pro_details.note as note')
            ->join('pro_details','products.id','=','pro_details.pro_id')
            ->where('products.id',$id)->first();


            return view('admin.product.edit',['pro'=>$pro,'cat'=>$cat,'loai'=>$p_id]);

    }
    public function postedit($id,Request $request)
    {
        $pro = Product::find($id);

        $pro->name = $request->name;
        $pro->slug = Str::slug($request->name,'-');
        $pro->intro = $request->intro;
        $pro->price = $request->price;
        $pro->cat_id = $request->sltCate;
        $pro->created_at = new datetime;
        $pro->status = '1';


        $file_path = public_path('uploads/products/') . $pro->images;

        if ($request->hasFile('img')) {
            if (file_exists($file_path))
            {
                unlink($file_path);
            }

            $f = $request->file('img')->getClientOriginalName();
            $filename = time().'_'.$f;
            $pro->images = $filename;
            $request->file('img')->move('uploads/products/',$filename);
        }
        $pro->save();
        $pro->pro_details->cpu = $request->cpu;
        $pro->pro_details->ram = $request->ram;
        $pro->pro_details->screen = $request->screen;
        $pro->pro_details->storage = $request->storage;
        $pro->pro_details->note = $request->note;



        $pro->pro_details->updated_at = new datetime;
        $pro->pro_details->save();

        return redirect('admin/product/list/all')
            ->with(['flash_level'=>'result_msg','flash_massage'=>' Đã lưu !']);
    }
    public function del($id)
    {
        $pro = Product::find($id);
        $pro->delete();
        return redirect('admin/product/list/all')
            ->with(['flash_level'=>'result_msg','flash_massage'=>'Đã xóa !']);
    }
}
