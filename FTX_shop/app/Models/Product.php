<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function pro_details()
    {
        return $this->hasOne('App\Models\Pro_detail', 'pro_id');
    }
}
