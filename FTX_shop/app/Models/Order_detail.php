<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order_detail extends Model
{
    public $timestamps =false;
    protected $table='order_details';
    protected $fillable=[
        'order_id','pro_id','qty','created_at'
    ];

}
