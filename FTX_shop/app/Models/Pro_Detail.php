<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pro_Detail extends Model
{
    protected $table ='pro_details';
    protected $guarded =[];
}
