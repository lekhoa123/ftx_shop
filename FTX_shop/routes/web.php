<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\NewController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OtherController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/admin')->group(function (){
    Route::get('/login',[AdminController::class,'showFormLogin'])->name('admin.login');
    Route::post('/login',[AdminController::class,'postLogin'])->name('admin.login');
    Route::get('/logout',[UserController::class,'logout'])->name('admin.logout');

    Route::middleware(['authCheck'])->group(function() {
        Route::get('',[AdminController::class,'home'])->name('admin.home');

//Quản lý danh mục
        Route::prefix('category')->group(function (){
            Route::get('list',[CategoryController::class,'list'])->name('category.list');
            Route::get('add',[CategoryController::class,'add'])->name('category.add');
            Route::post('add',[CategoryController::class,'postAdd'])->name('category.add');
            Route::get('edit/{id}',[CategoryController::class,'edit'])->name('category.edit');
            Route::post('edit/{id}',[CategoryController::class,'postEdit'])->name('category.edit');
            Route::get('/del/{id}',[CategoryController::class,'del'])->name('category.del');
        });
        //Quản lý sản phẩm
        Route::prefix('product')->group(function (){
            Route::get('list/{loai}',[ProductController::class,'list'])->name('product.list');
            Route::get('/{loai}/add',[ProductController::class,'add'])->name('product.add');
            Route::post('/{loai}/add',[ProductController::class,'postAdd'])->name('product.add');

            Route::get('/edit/{id}',[ProductController::class,'edit'])->name('product.edit');
            Route::post('/edit/{id}',[ProductController::class,'postEdit'])->name('product.edit');
            Route::get('/del/{id}',[ProductController::class,'del'])->name('product.del');
        });
        //Quản lý danh mục tin tức
        Route::prefix('new')->group(function (){
            Route::get('list',[NewController::class,'list'])->name('new.list');
            Route::get('add',[NewController::class,'add'])->name('new.add');
            Route::post('add',[NewController::class,'postAdd'])->name('new.add');
            Route::get('/edit/{id}',[NewController::class,'edit'])->name('new.edit');
            Route::post('/edit/{id}',[NewController::class,'postEdit'])->name('new.edit');
            Route::get('/del/{id}',[NewController::class,'del'])->name('new.del');
        });

        //Quản lý nhân viên
        Route::prefix('admin')->group(function (){
            Route::get('list',[AdminController::class,'list'])->name('admin.list');
        });
         //Quản lý khách hàng
        Route::prefix('user')->group(function (){
            Route::get('/list',[UserController::class,'list'])->name('admin.user.list');
        });
        //Quản lý đơn hàng
        Route::prefix('order')->group(function (){
            Route::get('/list',[OrderController::class,'list'])->name('admin.order.list');
            Route::get('/edit/{id}',[OrderController::class,'edit'])->name('admin.order.edit');
            Route::post('/edit/{id}',[OrderController::class,'postEdit'])->name('admin.order.edit');
        });

        //Thống kê
        Route::prefix('other')->group(function (){
            Route::get('/pro_selling',[OtherController::class,'pro_selling'])->name('admin.other.proSelling');
            Route::get('/user_total_qty',[OtherController::class,'qty_buy'])->name('admin.other.qtyBuy');
            Route::get('/user_total_money',[OtherController::class,'money_buy'])->name('admin.other.moneyBuy');
        });

    });
});
Route::prefix('')->group(function (){
    Route::get('',[HomeController::class,'index'])->name('home');
    Route::get('/page/{id}',[HomeController::class,'pagecat'])->name('page');

    Route::get('/pageNew',[HomeController::class,'pagenew'])->name('page.new');
});


Route::prefix('/user')->group(function () {
    Route::get('/register',[UserController::class,'register'])->name('user.register');
    Route::post('/register',[UserController::class,'postRegister'])->name('user.register');

    Route::get('/login',[UserController::class,'login'])->name('user.login');
    Route::post('/login',[UserController::class,'postLogin'])->name('user.login');
    Route::get('/logout',[UserController::class,'logout'])->name('user.logout');
    Route::get('/info',[UserController::class,'info'])->name('user.info');


    Route::group(['prefix'=>'cart'],function(){
        Route::get('view',[CartController::class,'view'])->name('cart.view');
        Route::get('add/{id}',[CartController::class,'add'])->name('cart.add');
        Route::get('remove/{id}',[CartController::class,'remove'])->name('cart.remove');
        Route::get('update/{id}',[CartController::class,'update'])->name('cart.update');
        Route::get('clear',[CartController::class,'clear'])->name('cart.clear');
    });
});
Route::middleware(['authCheckBuy'])->group(function() {
    Route::group(['prefix' => 'checkout'], function () {
        Route::get('/', [CheckoutController::class, 'form'])->name('checkout');
        Route::post('/', [CheckoutController::class, 'submit_form'])->name('checkout');

        Route::get('/chekout-success', [CheckoutController::class, 'success'])->name('checkout.success');

    });
});
