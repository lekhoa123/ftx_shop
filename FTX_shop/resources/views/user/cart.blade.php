@extends('layouts.user.master')
@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-home"><a href="#" title=""> Home</a></span>
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span><a href="#" title=""> Đặt hàng</a>
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span> <a href="#" title=""></a>
        </h3>
        <div >
            <div class="row">
                <div >
                    <div class="panel panel-success" style="min-height: 1760px;">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <div><a href="{{route('cart.clear')}}" class="btn-danger">Xóa hết</a> </div>

                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Hình ảnh</th>
                                        <th>Tên sản phẩm</th>
                                        <th style="text-align: center">SL</th>
                                        <th>Giá</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $n=1; ?>
                                    @foreach($cart->items as $k=>$item)
                                        <tr>
                                            <td>{{$n++}}</td>
                                            <td><img src="{!!url('uploads/products/'.$item['image'])!!}" alt="dell" width="80" height="50"></td>
                                            <td>{{$item['name']}}</td>
                                            <td style="text-align: center">
                                                <form action="{{route('cart.update',['id'=>$item['id']])}}">
                                                    <input type="number" name="quantity" value="{{$item['quantity']}}">
                                                    <input type="submit" class="btn-success" value="Update">
                                                </form>
                                            </td>
                                            <td>{{number_format($item['price'])}} Vnd</td>

                                            <td>{{number_format($item['price']*$item['quantity'])}} Vnd
                                            </td>

                                            <td><a href="{{route('cart.remove',['id'=>$item['id']])}}" type="button" class="btn btn-danger pull-right"> Xóa </a></td>=
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="3"><strong>Tổng cộng:</strong> </td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="2" style="color:red;">{{number_format($cart->total_price)}} Vnd</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                           @if($cart->total_quanty>0)
                            <a href="{{route('checkout')}}" title=""> <input type="submit" class="btn-success" value="Đặt hàng"></a>
                            @endif
                            <div class="col-xs-12 col-sm-12 col-md-12 no-paddng">

                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
