@extends('layouts.user.master')
@section('content')
    <form action=""  class="form-inline" style="float: right">

        <div class="form-group">
            <input  class="form-control" name="key5" placeholder="Tìm theo tên...">
        </div>
        <button type="submit" class="btn btn-primary">Tìm</button>
    </form>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div id="pc"></div>

        @foreach($pro as $p)
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding">
                <div class="thumbnail khac">
                    <div class="bt">
                        <div class="image-m pull-left" >
                            <img class="img-responsive" src="{!!url('uploads/products/'.$p->images)!!}" >
                        </div>
                        <div class="intro pull-right" >
                            <h1><small class="title-khac" style="color: blue">{{$p->name}}</small></h1>
                            <li> Giới thiệu: {{$p->intro}}</li>
                            <br>
                            <span class="label label-info" style="background: blue">FTX</span>

                        </div><!-- /div introl -->
                    </div> <!-- /div bt -->
                    <div class="ct">
                        <a href="" title="Xem chi tiết">
                            <span class="label label-warning" style="background: blue">Cấu hình chi tiết</span>
                            <li style="color: black"><strong>RAM</strong> : <i> {{$p->ram}} </i></li>
                            <li style="color: black"><strong>Màn hình</strong> : <i>{{$p->screen}} </i></li>
                            <li style="color: black"><strong>CPU </strong> :<i>{{$p->cpu}}</i></li>
                            <li style="color: black"><strong>Bộ nhớ:</strong> : <i> {{$p->storage}}</i></li>
                            <li style="color: black"><strong>Ghi chú</strong> : <i> {{$p->note}}</i></li>
                        </a>
                    </div>
                    <span class="btn label-warning" style="background: white; color: red"><strong>{{number_format($p->price)}}</strong> Vnd </span>
                    <a href="{{route('cart.add',['id' => $p->id])}}" class="btn btn-success pull-right add" style="background: orange">Thêm vào giỏ </a>
                </div> <!-- / div thumbnail -->
            </div>  <!-- /div col-4 -->

        @endforeach
        <!-- /div col-4 item products -->
    </div> <!-- /col 12 -->
    {!! $pro->links() !!}
@endsection

