@extends('layouts.user.master')
@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-home"><a href="#" title=""> Home</a></span>
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span><a href="#" title=""> Đặt hàng</a>
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span> <a href="#" title=""></a>
        </h3>
        <form action="" method="post" >
            @csrf
            <div >
                <div class="row">
                    <div >
                        <div class="panel panel-success" >

                            <h1 style="size: 20px">   Thông tin khách hàng</h1>
                            <div class="form-group" >
                                <label for="name">Tên: </label>
                                <input readonly=”readonly” value="{{$dataU->name}}">

                            </div>
                            <div class="form-group">
                                <label for="name">Email</label>
                                <input readonly=”readonly” value="{{$dataU->email}}">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div >
                        <div class="panel panel-success" >

                            <h1 style="size: 20px">   Thông tin mua hàng</h1>
                            @if($dataOr==null)
                              <div class="btn-danger">Chưa mua sản phẩm nào</div>
                                <br>
                            @else
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th>Tổng số sản phẩm</th>
                                                <th>Tổng tiền</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>{{$dataOr->tongsl}}</td>
                                                <td>{{ number_format($dataOr->tongtien)}} VNĐ</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 no-paddng">

                                    </div>
                                    <hr>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

