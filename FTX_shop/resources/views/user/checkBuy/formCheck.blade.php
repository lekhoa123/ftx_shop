@extends('layouts.user.master')
@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-home"><a href="#" title=""> Home</a></span>
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span><a href="#" title=""> Đặt hàng</a>
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span> <a href="#" title=""></a>
        </h3>
        <form action="" method="post" >
            @csrf
        <div >
            <div class="row">
                <div >
                    <div class="panel panel-success" >

                        <h1 style="size: 20px">   Thông tin khách hàng</h1>
                        <div class="form-group">
                            <label for="name">Tên </label>
                            <input type="text" class="form-control" placeholder="Email" name="name" value="{{$data->name}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Email</label>
                            <input type="text" class="form-control" placeholder="Email" name="email" value="{{$data->email}}">
                        </div>
                        <div class="form-group">
                            <label for="name">Địa chỉ</label>
                            <input type="text" class="form-control" placeholder="Địa chỉ" name="address" value="" required="required">
                        </div>
                        <div class="form-group">
                            <label for="name">Ghi chú</label>
                            <input type="text" class="form-control" placeholder="Ghi chú" name="note" value="" >
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div >
                    <div class="panel panel-success" >

                        <h1 style="size: 20px">   Thông tin sản phẩm</h1>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Hình ảnh</th>
                                        <th>Tên sản phẩm</th>
                                        <th style="text-align: center">SL</th>
                                        <th>Giá</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $n=1; ?>
                                    @foreach($cart->items as $k=>$item)
                                        <tr>
                                            <td>{{$n++}}</td>
                                            <td><img src="{!!url('uploads/products/'.$item['image'])!!}" alt="dell" width="80" height="50"></td>
                                            <td>{{$item['name']}}</td>
                                            <td style="text-align: center">

                                                <label>{{$item['quantity']}}</label>

                                            </td>
                                            <td>{{number_format($item['price'])}} Vnd</td>

                                            <td>{{number_format($item['price']*$item['quantity'])}} Vnd
                                            </td>

                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="3"><strong>Tổng cộng:</strong> </td>
                                        <td></td>
                                        <td></td>
                                        <td colspan="2" style="color:red;">{{number_format($cart->total_price)}} Vnd</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            @if($cart!='')
                            <a href="" title=""> <input type="submit" class="btn-success" value="Xác nhận"></a>
                            @endif
                            <div class="col-xs-12 col-sm-12 col-md-12 no-paddng">

                            </div>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    </div>
@endsection

