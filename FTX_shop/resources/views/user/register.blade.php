@extends('layouts.admin.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Đăng ký tài khoản mới</div>
                    <div class="panel-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">{{Session::get('success')}}</div>
                        @endif
                        @if(Session::has('fail'))
                            <div class="alert alert-danger">{{Session::get('fail')}}</div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="">
                            @csrf
                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Họ tên</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{old('name')}}">

                                </div>
                                <div>
                                    <span class="text-danger">@error('name') {{$message}} @enderror</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">Email</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{old('email')}}">

                                </div>
                                <div>
                                    <span class="text-danger">@error('email') {{$message}} @enderror</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password" class="col-md-4 control-label">Mật khẩu</label>

                                <div class="col-md-6">
                                    <input id="pass" type="password" class="form-control" name="pass" value="{{old('pass')}}">
                                </div>
                                <div>
                                    <span class="text-danger">@error('pass') {{$message}} @enderror</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-user"></i> Đăng ký
                                    </button>
                                </div>
                            </div>
                            <div>
                                <a href="{{route('user.login')}}">Đăng nhập</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

