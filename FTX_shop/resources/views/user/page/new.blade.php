@extends('layouts.user.master')
@section('content')
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h3 class="panel-title">
            <span class="glyphicon glyphicon-home"><a href="#" title=""> Home</a></span>
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span><a href="#" title=""> Tin Tức </a>
            <!--   <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span> <a href="#" title=""> noi dung con</a> -->
        </h3>
        <div>
                <div class="camera_violet_skin" id="camera_wrap_1">
                    <div data-thumb="{!!url('/images/slides/thumbs/quangcao1.png')!!}" data-src="{!!url('/images/slides/thumbs/quangcao1.png')!!}">
                        <div class="camera_caption fadeFromBottom">
                            Sắm máy tính giá tốt, Du lịch châu âu. <em>Chỉ có thể tại FTX</em>
                        </div>
                    </div>
                    <div data-thumb="{!!url('/images/slides/thumbs/quangcao2.png')!!}" data-src="{!!url('/images/slides/thumbs/quangcao2.png')!!}">
                        <div class="camera_caption fadeFromBottom">
                            Máy tính trả góp 0%, có cỏ hội trúng nhiều quà hấp dẫn <em> Dành cho máy tính tại FTX </em>
                        </div>
                    </div>
                    <div data-thumb="{!!url('/images/slides/thumbs/quangcao3.png')!!}" data-src="{!!url('/images/slides/thumbs/quangcao3.png')!!}">
                        <div class="camera_caption fadeFromBottom">
                            Máy tính - mua online thêm quà, <em> Dành cho khách hàng mua tại FTX </em>
                        </div>
                    </div>
                </div><!-- #camera_wrap_1 -->
            </div><!-- .fluid_container -->
        <div>
        @foreach($new as $n)
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="panel panel-success">
                        <div class="panel-body">
                          <h1>{{$n->title}}</h1>
                            <div class="row">
                                <!-- hot new content -->
                                <div class="col-lg-6">
                                    <img src="{!!url('uploads/news/'.$n->image)!!}" alt="" height="200" width="100%">
                                    <p class="summary-content">
                                    </p>
                                </div>
                                <div>{{$n->content}}</div>
                        </div>
                            Ngày tạo: <div>{{$n->created_at}}</div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        </div>
        {{$new->links()}}
    </div>

    <!-- ===================================================================================/news ============================== -->
@endsection
