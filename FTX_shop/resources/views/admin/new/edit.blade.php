@extends('layouts.admin.master')
@section('content')
    <!-- main content - noi dung chinh trong chu -->
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row" >
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active">Tin tức</li>
            </ol>
        </div><!--/.row-->

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><small>Sửa bản tin </small></h1>
            </div>
        </div><!--/.row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body" style="background-color: #ecf0f1; color:#27ae60;">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @elseif (Session()->has('flash_level'))
                            <div class="alert alert-success">
                                <ul>
                                    {!! Session::get('flash_massage') !!}
                                </ul>
                            </div>
                        @endif
                        <form action="" method="POST" role="form" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="input-id">Tiêu đề bản tin</label>
                                <input type="text" name="title" id="inputTxtTitle" class="form-control" value="{{$data->title}}" required="required">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        Hình ảnh : <input type="file" name="img" accept="image/*" id="img"  class="form-control" >
                                        Ảnh hiện tại:
                                        <br><img src="{!!url('uploads/news/'.$data->image)!!}" alt="" height="40" width="80">
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        Trạng thái : <select name="status" id="status" class="form-control" required="required">
                                            @if($data->status==1){
                                            <option value="1" selected>Hiển thị</option>
                                            <option value="0" >Tạm ẩn</option>
                                                }
                                            @endif
                                                @if($data->status=='0'){
                                                <option value="1" >Hiển thị</option>
                                                <option value="0" selected>Tạm ẩn</option>
                                                }
                                                @endif


                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="input-id">Bài viết chi tiết</label>
                                <textarea name="content" id="content" class="form-control" rows="4"
                                          required="required">{{$data->content}}</textarea>
                            </div>

                            <input type="submit" name="btnCateAdd" class="btn btn-primary" value="Sửa" class="button" />
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
    <!-- =====================================main content - noi dung chinh trong chu -->
@endsection
