@extends('layouts.admin.master')
@section('content')
    <!-- main content - noi dung chinh trong chu -->
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row" >
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active">Đơn đặt hàng</li>
            </ol>
        </div><!--/.row-->

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><small>Sửa đơn hàng </small></h1>
            </div>
        </div><!--/.row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body" style="background-color: #ecf0f1; color:#27ae60;">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @elseif (Session()->has('flash_level'))
                            <div class="alert alert-success">
                                <ul>
                                    {!! Session::get('flash_massage') !!}
                                </ul>
                            </div>
                        @endif
                        <form action="" method="POST" role="form" enctype="multipart/form-data">
                            @csrf
                            <div>
                            <div class="form-group">
                                <label for="input-id">Tên</label>
                                <input type="text" name="name"  class="form-control" value="{{$data->name}}" required="required">
                            </div>
                            <div class="form-group">
                                <label for="input-id">Email</label>
                                <input type="text" name="email" class="form-control" value="{{$data->email}}" required="required">
                            </div>
                            <div class="form-group">
                                <label for="input-id">Địa chỉ</label>
                                <input type="text" name="address"  class="form-control" value= {{$data->address}} required="required">
                            </div>
                            <div>

                                <div >
                                    Trạng thái : <select name="status" id="status" class="form-control"
                                                         required="required">
                                        @if($data->status==1)
                                            {
                                            <option value="1" selected>Hiển thị</option>
                                            <option value="0">Tạm ẩn</option>
                                            }
                                        @endif
                                        @if($data->status=='0')
                                            {
                                            <option value="1">Hiển thị</option>
                                            <option value="0" selected>Tạm ẩn</option>
                                            }
                                        @endif

                                    </select>
                                </div>
                            </div>

                            <input type="submit" name="btnCateAdd" class="btn btn-primary" value="Sửa" class="button" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
    <!-- =====================================main content - noi dung chinh trong chu -->
@endsection
