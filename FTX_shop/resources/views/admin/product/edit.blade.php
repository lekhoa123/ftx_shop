@extends('layouts.admin.master')
@section('content')
    <!-- main content - noi dung chinh trong chu -->
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="{{route('admin.home')}}"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active">Sản phẩm</li>
            </ol>
        </div><!--/.row-->

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><small>Sửa sản phẩm: {!!$loai!!}</small></h1>
            </div>
        </div><!--/.row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body" style="background-color: #ecf0f1; color:#27ae60;">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @elseif (Session()->has('flash_level'))
                            <div class="alert alert-success">
                                <ul>
                                    {!! Session::get('flash_massage') !!}
                                </ul>
                            </div>
                        @endif
                        <form action="" method="POST" role="form" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="input-id">Chọn danh mục</label>
                                <select name="sltCate" id="inputSltCate" required class="form-control">
                                    <option value="">--Chọn thương hiệu--</option>
                                    @foreach($cat as $dt)
                                        <option value="{!!$dt->id!!}" >{!!'--|--|'.$dt->name!!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="input-id">Tên sản phẩm</label>
                                <input type="text" name="name" id="name" class="form-control" value="{{$pro->name}}" required="required"  >
                            </div>
                            <div class="form-group">
                                <label for="input-id">Giới thiệu</label>
                                <input type="text" name="intro" id="intro" class="form-control" value="{{$pro->intro}}" required="required">
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        <input type="file" name="img" accept="image/png"  class="form-control" >
                                        Hình ảnh : Ảnh cũ: <img src="{!!url('uploads/products/'.$pro->images)!!}" alt="{!!$pro->images!!}" width="80" height="60">
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        Giá bán : <input type="number" name="price" id="inputtxtprice" class="form-control" value="{{$pro->price}}" required="required">
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                            </div>
                            <div class="form-group">
                                <label for="input-id">Đặc điểm sản phẩm</label>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        Ram : <input type="text" name="ram" id="ram" value="{{$pro->ram}}"class="form-control" required="required">
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        Màn hình <input type="text" name="screen" id="screen" value="{{$pro->screen}}" class="form-control" required="required" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                        CPU : <input type="text" name="cpu" value="{{$pro->cpu}}" class="form-control" required="required" >
                                    </div>
                                    <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                                        Bộ nhớ <input type="text" name="storage"  value="{{$pro->storage}}" class="form-control" required="required" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="input-id">Chú thích</label>
                                <input type="text" name="note"  class="form-control" value="{{$pro->note}}" required="required">
                            </div>

                            <div>
                            </div>

                            <input type="submit" class="btn btn-primary" value="Sửa" class="button" />
                        </form>
                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
    <!-- =====================================main content - noi dung chinh trong chu -->
@endsection
