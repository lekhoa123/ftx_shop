@extends('layouts.admin.master')
@section('content')
    <!-- main content - noi dung chinh trong chu -->
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
                <li class="active">Sản phẩm</li>
            </ol>
        </div><!--/.row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <label for="inputLoai" class="col-sm-3 control-label"><strong style="color: #0f74a8"> SẢN PHẨM BÁN CHẠY </strong></label>
                            <form action="{{route('admin.other.proSelling')}}"  class="form-inline" >

                                <div class="form-group">
                                    <input  class="form-control" name="key2" placeholder="Nhập năm">
                                </div>
                                <button type="submit" class="btn btn-primary">Tìm</button>
                            </form>
                        </div>
                    </div>

                    <div class="panel-body" style="font-size: 12px;">
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Ảnh</th>
                                    <th>Tổng số lượng bán</th>

                                </tr>
                                </thead>
                                <tbody>
                            @foreach($data as $p)
                                    <tr>
                                        <td>{{$p->id}}</td>
                                        <td>{{$p->name}} </td>
                                        <td> <img src="{!!url('uploads/products/'.$p->images)!!}" alt="iphone" width="50" height="40"></td>
                                        <td>{{$p->tongsl}} </td>
                                    </tr>
                            @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div><!--/.row-->
    </div>	<!--/.main-->
    <!-- =====================================main content - noi dung chinh trong chu -->
@endsection
