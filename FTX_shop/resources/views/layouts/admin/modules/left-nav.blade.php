<!-- left menu - menu ben  trai	 -->
<div style="margin-top: -50px;background: darkgray" id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar" >

    <ul class="nav menu" style="background-color: darkgray">
        <li class="active"  ><a href="{{route('admin.home')}}"><svg class="glyph stroked dashboard-dial" ><use xlink:href="#stroked-dashboard-dial"  ></use></svg> Trang chủ </a></li>
        <li id="danhmuc"><a href="{{route('category.list')}}"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Danh mục</a></li>

        <li id="sanpham"><a href="{{url('admin/product/list/all')}}"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg> Sản phẩm </a></li>
        <li><a href="{{route('new.list')}}"><span class="glyphicon glyphicon-file"></span> Tin tức</a></li>

        {{-- <li><a href="{!!url('admin/nhaphang')!!}"><svg class="glyph stroked download"><use xlink:href="#stroked-download"/></svg> Nhập hàng</a></li> --}}

        <li><a href="{{route('admin.order.list')}}"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg> Đơn đặt hàng</a></li>

        <li><a href="{{route('admin.user.list')}}"><svg class="glyph stroked app-window"><use xlink:href="#stroked-line-graph"></use></svg>  Khách hàng</a></li>

        <li><a href="{{route('admin.list')}}"><svg class="glyph stroked female user"><use xlink:href="#stroked-female-user"/></svg> Nhân Viên</a></li>

        <li role="presentation" class="divider"></li>
        <li><a href="{{route('admin.other.proSelling')}}"><svg class="glyph stroked notepad "><use xlink:href="#stroked-notepad"/></svg>  Sản phẩm bán chạy</a></li>
        <li><a href="{{route('admin.other.qtyBuy')}}"><svg class="glyph stroked notepad "><use xlink:href="#stroked-notepad"/></svg>  Khách hàng mua nhiều sản phẩm</a></li>
        <li><a href="{{route('admin.other.moneyBuy')}}"><svg class="glyph stroked table"><use xlink:href="#stroked-table"/></svg> Khách hàng chi nhiều tiền </a></li>
    </ul>

</div><!--/.sidebar-->
<!-- /left menu - menu ben  trai	 -->
