@include('layouts.user.header')
@include('layouts.user.menu')
@include('layouts.user.slide')
<div class="container">
    <div class="row">
        @yield('content')
        @include('layouts.user.gioithieu')
    </div>       <!-- /row -->
</div> <!-- /container -->
@include('layouts.user.footer')
