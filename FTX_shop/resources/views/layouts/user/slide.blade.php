<!-- slider event - top -->
<div class="container hidden-xs">
    <div class="fluid_container">
        <div class="camera_violet_skin" id="camera_wrap_1">
            <div data-thumb="{!!url('/images/slides/thumbs/quangcao1.png')!!}" data-src="{!!url('/images/slides/thumbs/quangcao1.png')!!}">
                <div class="camera_caption fadeFromBottom">
                    Sắm máy tính giá tốt, Du lịch châu âu. <em>Chỉ có thể tại FTX</em>
                </div>
            </div>
            <div data-thumb="{!!url('/images/slides/thumbs/quangcao2.png')!!}" data-src="{!!url('/images/slides/thumbs/quangcao2.png')!!}">
                <div class="camera_caption fadeFromBottom">
                    Máy tính trả góp 0%, có cỏ hội trúng nhiều quà hấp dẫn <em> Dành cho máy tính tại FTX </em>
                </div>
            </div>
            <div data-thumb="{!!url('/images/slides/thumbs/quangcao3.png')!!}" data-src="{!!url('/images/slides/thumbs/quangcao3.png')!!}">
                <div class="camera_caption fadeFromBottom">
                    Máy tính - mua online thêm quà, <em> Dành cho khách hàng mua tại FTX </em>
                </div>

            </div><!-- #camera_wrap_1 -->
        </div><!-- .fluid_container -->
    </div> <!-- /slider event - top -->
