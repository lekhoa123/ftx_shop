<!-- main menu  navbar -->
<nav class="navbar navbar-default navbar-top" style="background: black; "  role="navigation" id="main-Nav" style="background-color: cornflowerblue;margin-bottom: 5px;font-size: 13px;">
    <div class="container">
        <div class="row">
            <!-- Brand and toggle get grouped for better khac display -->
            <div class="navbar-header">
                <span  class="visible-xs pull-left" style="font-size:30px;cursor:pointer; padding-left: 10px; color: #ecf0f1;" onclick="openNav()">&#9776; </span>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-mav-top">
                <ul class="nav navbar-nav">
                    <li> <a href="{{route('home')}}" title="" style="color: #FFFFFF;background-color: #2c3e50;"><b class="glyphicon glyphicon-home"></b> TRANG CHỦ </a> </li>
{{--//Lấy danh sách danh mục--}}
                    @foreach($listcat as $c)
                        <li>
                            <a style="text-transform: uppercase" href="{!!url('/page/'.$c->id)!!}">{{$c->name}}</a>
                        </li>
                    @endforeach
                    <li>
                        <a href="{{route('page.new')}}" > TIN TỨC</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a href="{{route('cart.view')}}">
                            <span class="glyphicon glyphicon-shopping-cart">
                               ({{$cart->total_quanty}})
                            </span>
                        </a>
                    </li>
                    <!-- Authentication Links -->
                    @if(!Session::has('LoginId'))
                        <li class="dropdown">

                   <a href="{{route('user.login')}}" >Đăng nhập</a>
                        </li>
{{--                        <a style="margin-top: 10px" href="{{route('user.login')}}" data-toggle="modal" data-target="#login-modal" style="color:#e67e22;"> Đăng nhập </a>--}}
                        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="text-transform: uppercase">
                               {{$datau->name}}
                                    <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{route('user.info')}}">Thông tin cá nhân</a></li>
                                <li><a href="{{route('user.logout')}}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div><!-- /.navbar-collapse -->
        </div> <!-- /row -->
    </div><!-- /container -->
</nav>    <!-- /main nav -->

<!-- left slider bar nav -->
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times; Đóng</a>
    <a href="{!!url('pc')!!}">Máy Tính</a>
    <a href="{!!url('khac')!!}">Khac</a>
    <a href="{!!url('laptop')!!}">Laptop</a>
    <a href="{!!url('tin-tuc')!!}">Tin Tức</a>
    <a href="{!!url('gio-hang')!!}"> <span class="glyphicon glyphicon-shopping-cart"><span class="badge"></span></span> Giỏ Hàng </a>
</div>
<!-- /left slider bar nav -->

{{-- loginform --}}
